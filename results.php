<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
<!--    <title>NFCS FUTO ELECTION 2018</title>-->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/main.css">
    <script type="text/javascript" src="assets/js/jquery3.1.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/loading.css">
</head>
<body style="background: #1c1d26; color: rgba(255, 255, 255, 0.75);">
<div class="content">
    <div class="container">
        <div style="padding-top: 10%">
<!--            <h1> NFCS FUTO ELECTION 2018 - RESULTS</h1>-->
            <div class="" style="padding-top: 5%">
                <form action="/process_vote.php" method="post" id='form' >
                    <div class="form-group">
                        <label for="positions">Position</label>
                        <select class="form-control" id="positions" name="position">
                            <option value="president" id="president" selected>President</option>
                            <option value="vicepresident" id="vicepresident" > Vice President</option>
                            <option value="secgen" id="secgen"  >Secretary General</option>
                            <option value="asg"  id="asg">Assistant Secretary General</option>
                            <option value="finsec" id="finsec" >Financial Secretary</option>
                            <option value="treasurer" id="treasurer" >Treasurer</option>
                            <option value="pro1" id="pro1" >P.R.O 1</option>
                            <option value="pro2"  id="pro2">P.R.O 2</option>
                            <option value="provost1" id="provost1" >Provost 1</option>
                            <option value="provost2" id="provost2" >Provost 2</option>
                            <option value="librarian" id="librarian" >Librarian</option>
                            <option value="coordinator1" id="coordinator1" >Coordinator 1</option>
                            <option value="coordinator2" id="coordinator2" >Coordinator 2</option>
                        </select>
                    </div>
                    <h2 id="results_for"> <!--for insertion of position currently been voted --></h2>
                    <div class="form-group">

                        <input type="button" class="btn btn-primary" id="see_results" value="See Results">
                        <a class="btn btn-primary" href="index.php">Home</a>
                    </div>
                    <table class="table table-dark table-lg" style="border: 0; font-size: 160px;" id='resultsTable'>
                        <tbody>

                        </tbody>
                    </table>
            </div>
            <div class="overlay" style="display: none;">
                <div class="ld  ld-hourglass ld-spin-fast" id="loader"></div>
            </div>
        </div>
    </div>
</div>
<footer class="footer">
    © Copyright 2018, All Rights Reserved. &ensp; Developed by Kris. &ensp; Content by Phoenix.
</footer>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src='assets/js/main.js'></script>
<script>
    $( "#positions" )
        .change(function() {
            var str = $("select option:selected").text();
            var voted_position_element = $("#results_for");
            if (voted_position_element.text() !== str){
                voted_position_element.fadeOut( "slow", "linear" );
                voted_position_element.fadeIn();
                voted_position_element.text("Results for " + str);
            }
        })
        .trigger( "change" );

    $(document).ready(function() {
            $("#see_results").click(function(event){
                renderLoader();
                var position = $("select option:selected").val();
                $("#resultsTable").load('/process_result.php', {"position":position} );
            });
        });
</script>
</body>
</html>
