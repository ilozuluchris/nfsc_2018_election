<?php
$servername = "localhost";
$username = "root";
$password = "112233";
$dbname = "nfcs_2018_election";



function get_choice($choice) {
    return "{$choice}";
}

function check_choice_a($choice){
    return $choice=='A';

}
function check_choice_b($choice){
    return $choice=='B';

}
function check_choice_c($choice){
    return $choice=='C';

}

// todo doesnot handle tie cases where a and b or b and c  are the same.
function style_biggest_vote($a,$b,$c){
    $max = max(Array($a,$b,$c));

    switch($max){
        case $a:
            $string_a = "<tr style='color: #2e6da4;'><td> A </td><td>". $a . "</td></tr>";
            $string_b = "<tr><td> B </td><td>". $b . "</td></tr>";
            $string_c = "<tr><td> C </td><td>". $c . "</td></tr>";
            break;
        case $b:
            $string_a = "<tr><td> A </td><td>". $a . "</td></tr>";
            $string_b = "<tr style='color: #2e6da4;'><td> B </td><td>". $b . "</td></tr>";
            $string_c = "<tr><td> C </td><td>". $c . "</td></tr>";
            break;
        case $c:
            $string_a = "<tr><td> A </td><td>". $a . "</td></tr>";
            $string_b = "<tr><td> B </td><td>". $b . "</td></tr>";
            $string_c = "<tr style='color: #2e6da4;'><td> C </td><td>". $c . "</td></tr>";
            break;
    }
    return $string_a.$string_b.$string_c;
}

function Results_string($array){

    $a_count = count(array_filter($array,'check_choice_a'));
    $b_count = count(array_filter($array, 'check_choice_b'));
    $c_count = count(array_filter($array,'check_choice_c'));

    $total_result_string = style_biggest_vote($a_count,$b_count,$c_count);
    return $total_result_string;

}


function count_votes($servername = "localhost", $username = "root", $password = "112233", $dbname = "nfsc_2018_election",$position_votes_to_get){
    try {
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $query = $conn->prepare("SELECT choice FROM votes WHERE position=:position");
        $query->bindParam(':position', $position);
        $position = $position_votes_to_get;
        $query->execute();

        // set the resulting array to associative
        $result = Results_string($query->fetchAll(PDO::FETCH_FUNC, "get_choice"));
    }
    catch(PDOException $e) {
//        echo "Error: " . $e->getMessage();
        $result = "Error: " . $e->getMessage();
    }
    $conn = null;
    return $result;
}


function store_vote($servername = "localhost", $username = "root", $password = "112233", $dbname = "nfcs_2018_election", $position, $choice){
    try {
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // prepare sql and bind parameters
        $query = $conn->prepare("INSERT INTO votes (position, choice) VALUES (:position, :choice)");
        $query->bindParam(':position', $position_to_save);
        $query->bindParam(':choice', $choice_to_save);

        $position_to_save = $position;
        $choice_to_save = $choice;

        $query->execute();


    }
    catch(PDOException $e)
    {
        $hf= "Error: " . $e->getMessage();
    }
    $conn = null;
}