import secrets
import string
import sys


def participants_numbering_to_lst():
    participants_numbering = input("Enter comma seperated values. eg 1,2,3: ")
    participants_numbering_split = participants_numbering.split(",")
    num_lst = [int(string) for string in participants_numbering_split]
    return num_lst


def get_participants_numbering():
   participants_numbering_lst = participants_numbering_to_lst()
   alphabets = string.ascii_lowercase
   participants_row_column_list = []
   for participants_num in participants_numbering_lst:
        alphabet_to_prepend = alphabets[participants_numbering_lst.index(participants_num)]
        for num in range(1, participants_num+1):
            participants_row_column_list.append(alphabet_to_prepend+str(num))
   return participants_row_column_list


def choose_num(num_list):
    if num_list:
        current_num_lst = list(num_list)
        answer = input("Type in y to choose a number: ")
        if answer == "y":
            print("Choosing number")
            chosen_num = secrets.choice(current_num_lst)
            print ("Choosen number is " + str(chosen_num))
            chosen_num_index = current_num_lst.index(chosen_num)
            del current_num_lst[chosen_num_index]
            choose_num(current_num_lst)
        else:
            choose_num(current_num_lst)
    else:
        sys.exit("Can not handle empty lists")


def pick_random_participant():
    participants_row_column_list = get_participants_numbering()
    choose_num(participants_row_column_list)


if __name__ == "__main__":
    pick_random_participant()
