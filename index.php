<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
<!--	<title>NFCS FUTO ELECTION 2018</title>-->
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/main.css">
    <script src="assets/js/sweetalert.min.js"></script>
</head>
<body style="background: #1c1d26; color: rgba(255, 255, 255, 0.75);">
	<div class="content">
		<div class="container">
			<div style="padding-top: 10%">
<!-- todo			    <h1> NFCS FUTO ELECTION 2018</h1>-->
<?php
require 'database.php';
if (isset($_POST['position'])){
    $position_array = Array(
            'president'=>'President',
            "vicepresident" =>  "Vice President",
            "secgen" => "Secretary General",
            "asg"=> "Assistant Secretary General",
            "finsec"  =>"Financial Secretary",
            "treasurer"  =>"Treasurer",
            "pro1"  => "P.R.O 1",
            "pro2"  =>"P.R.O 2",
            "provost1"  => "Provost 1",
            "provost2"  => "Provost 2",
            "librarian"  => "Librarian",
            "coordinator1"  => "Coordinator 1",
            "coordinator2"  => "Coordinator 2",
    );
    $position_to_save = $_POST['position'];
    $choice_to_save = $_POST['option_voted'];
    store_vote($servername = "localhost", $username = "root", $password = "112233",$dbname = "nfcs_election_2018", $position=$position_to_save, $choice=$choice_to_save );
    //todo adding back or home button, change to sweet alerts and move to index page
    echo '<script type="text/javascript">swal("Your vote for '. $position_array[$position_to_save].' has been saved.");</script>';
}?>
                <div class="" style="padding-top: 5%">
                    <form action="" method="post" id='form'>
                        <div class="form-group">
                            <label for="position_to_vote">Position</label>
                            <select class="form-control" id="position_to_vote" name="position">
                                <option value="president" id="president">President</option>
                                <option value="vicepresident" id="vicepresident" > Vice President</option>
                                <option value="secgen" id="secgen"  >Secretary General</option>
                                <option value="asg"  id="asg" >Assistant Secretary General</option>
                                <option value="finsec" id="finsec" >Financial Secretary</option>
                                <option value="treasurer" id="treasurer" >Treasurer</option>
                                <option value="pro1" id="pro1" >P.R.O 1</option>
                                <option value="pro2"  id="pro2">P.R.O 2</option>
                                <option value="provost1" id="provost1" >Provost 1</option>
                                <option value="provost2" id="provost2" >Provost 2</option>
                                <option value="librarian" id="librarian" >Librarian</option>
                                <option value="coordinator1" id="coordinator1" >Coordinator 1</option>
                                <option value="coordinator2" id="coordinator2" >Coordinator 2</option>
                            </select>
                        </div>
                        <h2 id="vote_for"> <!--for insertion of position currently been voted --></h2>
                        <div class="form-group">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="option_voted" id="option1" value="A" required="required">A
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="option_voted" id="option2" value="B">B
                                </label>
                            </div>
                            <div class="form-check disabled">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="option_voted" id="option3" value="C" >C
                                </label>
                            </div>
                            <button type="submit" class="btn btn-primary"><strong>VOTE</strong></button>
    </div>
</div>

<!---->
<!--                <div class="alert alert-success fade in" id="vote_sucess">-->
<!---->
<!--                    <a href="#" class="close" data-dismiss="alert">&times;</a>-->
<!---->
<!--                    <strong>Success!</strong> Your message has been sent successfully.-->
<!---->
<!--                </div>-->


            </div>
		</div>
	</div>
	 <footer class="footer">
         © Copyright 2018, All Rights Reserved. &ensp; Developed by Kris. &ensp; Content by Phoenix. 
      </footer>
	<script type="text/javascript" src="assets/js/jquery3.1.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src='assets/js/main.js'></script>
    <script>
        $( "#position_to_vote" )
        .change(function() {
            var str = $("select option:selected").text();
            var voted_position_element = $("#vote_for");
            if (voted_position_element.text() !== str){
                voted_position_element.fadeOut( "slow", "linear" );
                voted_position_element.fadeIn();
                voted_position_element.text("Vote For " + str);
            }
        })
        .trigger( "change" );
    </script>
<?php
if (isset($_POST['position'])){
        echo '<script type="text/javascript">
        $( document ).ready(
            function() {$("#position_to_vote").val("'.$_POST["position"].'");
            fadeSelection();
            });</script>';
    }
?>

</body>
</html>
