/** Check if there is a participant number list.**/
function isThereParticipantNumberList() {
    return JSON.parse(localStorage.getItem('participant_no_list'));
}


/** Save new participant number list if there is none. **/
function putParticipantNumberList(){
    var length_of_list = document.getElementById('no_of_participants').value;
    var first_participant_no_list =  [...Array(Number(length_of_list)).keys()]; // dont change, this works.
    localStorage.setItem('participant_no_list', JSON.stringify(first_participant_no_list));
}


/**Pick a random number from the participant number list**/
function PickRandomParticipant() {
    var current_participant_no_list = JSON.parse(localStorage.getItem('participant_no_list'));
    return current_participant_no_list[Math.floor(Math.random() * current_participant_no_list.length)];
}


function saveNewParticipantNumberList(participant_no_list){
    localStorage.setItem('participant_no_list', JSON.stringify(participant_no_list));
}


function changeParticipantNumberListAfterPicking(number_picked){
    var participant_no_list = JSON.parse(localStorage.getItem('participant_no_list'));
    var number_picked_index = participant_no_list.indexOf(number_picked);
    participant_no_list.splice(number_picked_index,1);
    return participant_no_list;
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}




async function renderLoader(){
    $('.overlay').show();
    var random_times =Math.floor(Math.random() * 5);// todo what if it is zero
    var random_times2 =Math.floor(Math.random() * 5);
    var animation = $('#loader');
    // for (i=1;i<random_times;i++){



    animation.attr("style", "font-size:200px; color:#8da; animation-play-state:running");
    await sleep(random_times2*1000);
    animation.attr("style", "font-size:200px; color:#8da; animation-play-state:paused");
    await sleep(random_times*1000);
    animation.attr("style", "font-size:200px; color:#8da; animation-play-state:running");
    await sleep(1000);
    $('.overlay').hide();
}

function displayPickedParticipant(picked_participant) {
    document.getElementById('picked_participant').innerText = picked_participant+1;
}
/**todo lastthoughts need to under async and promises. problem with for loop**/
/** Does the actual processing called by the clicking the pick participant buttont**/
async function processPicking() {
    //make number input read only
    renderLoader();
    if (! isThereParticipantNumberList()){
        putParticipantNumberList();
    }
    var picked_participant = PickRandomParticipant();  //todo: number to shown on front end should be + 1 of this
    console.log(picked_participant);
    var new_participant_no_list = changeParticipantNumberListAfterPicking(picked_participant);
    saveNewParticipantNumberList(new_participant_no_list);
    displayPickedParticipant(picked_participant);
}


function fadeSelection(){
    $( "#position_to_vote" )
        .change(function() {
            var str = $("select option:selected").text();
            var voted_position_element = $("#vote_for");
            if (voted_position_element.text() !== str){
                voted_position_element.fadeOut( "slow", "linear" );
                voted_position_element.fadeIn();
                voted_position_element.text("Vote For " + str);
            }
        })
        .trigger( "change" );
}